#!bin/bash
# https://losst.pro/ustanovka-i-nastrojka-prometheus
mkdir graf
cd graf/
sudo apt-get install -y adduser libfontconfig1 musl
wget https://dl.grafana.com/oss/release/grafana_10.2.0_amd64.deb
sudo dpkg -i grafana_10.2.0_amd64.deb
systemctl status grafana-server
systemctl status grafana-server
systemctl start grafana-server
systemctl status grafana-server
apt install mc -y
mkdir prom
cd prom/
wget https://github.com/prometheus/prometheus/releases/download/v2.47.2/prometheus-2.47.2.linux-amd64.tar.gz
tar xvf prometheus-2.47.2.linux-amd64.tar.gz
cd prometheus-2.47.2.linux-amd64
nano prometheus.yml
prometheus
./prometheus
